/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tictactoe;

import java.util.Scanner;

/**
 *
 * @author Aritsu
 */
public class OX {

    public static void printBoard(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static char checkWin(char[][] board) {

        for (int i = 0; i < 3; i++) {
            if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != '-') {
                return board[i][0];
            }
        }

        for (int j = 0; j < 3; j++) {
            if (board[0][j] == board[1][j] && board[1][j] == board[2][j] && board[0][j] != '-') {
                return board[0][j];
            }
        }

        if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != '-') {
            return board[0][0];
        }

        if (board[2][0] == board[1][1] && board[1][1] == board[0][2] && board[2][0] != '-') {
            return board[2][0];
        }

        return ' ';
    }

    public static boolean fullBoard(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.println("Welcome to OX Game");
        char[][] board = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }

        boolean playerO = true;
        boolean status = false;
        while (!status) {
            if (playerO) {
                System.out.println("Turn O");
            } else {
                System.out.println("Turn X");
            }

            char dat = '-';
            if (playerO) {
                dat = 'O';
            } else {
                dat = 'X';
            }
            int row = 0, col = 0;

            while (true) {
                System.out.println("Please input row, col:");
                row = s.nextInt();
                col = s.nextInt();

                if (board[row][col] != '-') {
                    System.out.println("This position has been already set");
                } else {
                    break;
                }
            }
            board[row][col] = dat;
            if (checkWin(board) == 'O') {
                System.out.println(">>>O Win<<<");
                status = true;
            } else if (checkWin(board) == 'X') {
                System.out.println(">>>X Win<<<");
                status = true;
            } else {
                if (fullBoard(board)) {
                    System.out.println(">>>Draw<<<");
                    status = true;
                } else {
                    playerO = !playerO;
                }
            }

            printBoard(board);
        }

    }
}
